const requestId = require('../helper/requestId')
const dialogFlowEntityModel = require('../models/dialogFlowEntityModel');

const createNewEntity = async(req, res) => {
    var request_id = requestId.requestId(req.params.company);
    try {
        var response = await dialogFlowEntityModel.createNewEntity(request_id, req.body.displayName, req.body.entities);
        res.status(200).send(response);
    } catch (e) {
        console.log("req=" + request_id.id);
        console.log(e);
    }
}

const getEntity = async(req, res) => {
    var request_id = requestId.requestId(req.params.company);
    try {
        var response = await dialogFlowEntityModel.getEntity(request_id, req.params.displayName);
        res.status(200).send(response);
    } catch (e) {
        console.log("req=" + request_id.id);
        console.log(e);
        res.status(404).end();
    }
}

const getAllEntity = async(req, res) => {
    var request_id = requestId.requestId(req.params.company);
    try {
        var response = await dialogFlowEntityModel.getAllEntity(request_id);
        res.status(200).send(response);
    } catch (e) {
        console.log("req=" + request_id.id);
        console.log(e);
        res.status(404).end();
    }
}

const updateEntity = async(req, res) => {
    var request_id = requestId.requestId(req.params.company);
    try {
        var response = await dialogFlowEntityModel.updateEntity(request_id, req.body.displayName, req.body.entities);
        res.status(200).send(response);
    } catch (e) {
        console.log("req=" + request_id.id);
        console.log(e);
        res.status(404).end();
    }
}

const deleteEntity = async(req, res) => {
    var request_id = requestId.requestId(req.params.company);
    try {
        var response = await dialogFlowEntityModel.deleteEntity(request_id, req.body.displayName);
        res.status(200).send(response);
    } catch (e) {
        console.log("req=" + request_id.id);
        console.log(e);
        res.status(404).end();
    }
}

module.exports = {
    createNewEntity,
    getEntity,
    getAllEntity,
    updateEntity,
    deleteEntity
}