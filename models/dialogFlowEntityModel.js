const dialogflow = require('dialogflow');
const dao = require('../dao/baseDao');
class EntityNotFoundError extends Error {};

const createNewEntity = async(req, displayName, entityList) => {
    return new Promise(async(resolve, reject) => {
        var dialog_flow_data = await dialog_flow_connection(req.company);
        const entityType = {
            displayName: displayName,
            kind: 'KIND_MAP',
            entities: entityList,
        };

        const request = {
            parent: dialog_flow_data.agentPath,
            entityType: entityType,
        };
        dialog_flow_data.entitiesClient.createEntityType(request).then((responses) => {
            console.log("req=" + req.id + ' Created new entity type:', JSON.stringify(responses[0]));
            resolve(responses[0]);
        }).catch((err) => {
            if (err.code == 9) {
                console.log("req=" + req.id + ' Entity already exist.');
                resolve({ msg: displayName + " entity already exist" });
            } else {
                console.error("req=" + req.id + ' Error create entity type: ', err);
                reject(err);
            }
        });
    });
}

const getEntity = async(req, displayName) => {
    return new Promise(async(resolve, reject) => {
        var dialog_flow_data = await dialog_flow_connection(req.company);
        dialog_flow_data.entitiesClient.listEntityTypes({ parent: dialog_flow_data.agentPath }).then((responses) => {
            // The array of EntityTypes is the 0th element of the response.
            const resources = responses[0];
            for (let i = 0; i < resources.length; i++) {
                const entity = resources[i];
                console.log("req=" + req.id + " entity:", entity);
                if (entity.displayName == displayName) {
                    resolve(entity);
                    return;
                }
            }
            throw new EntityNotFoundError();
        }).catch((err) => {
            console.log(typeof err)
            if (err == "Error: " + displayName) {
                console.error("req=" + req.id + ' Could not find the entity named ' + displayName + ".");
                reject(err);
            } else {
                console.error("req=" + req.id + ' Error getting entity type: ', err);
                reject(err);
            }
        });
    });
}

const getAllEntity = async(req) => {
    return new Promise(async(resolve, reject) => {
        var dialog_flow_data = await dialog_flow_connection(req.company);
        dialog_flow_data.entitiesClient.listEntityTypes({ parent: dialog_flow_data.agentPath }).then((responses) => {
            // The array of EntityTypes is the 0th element of the response.
            console.log("req=" + req.id + " all entities: ", responses);
            resolve(responses[0]);
        }).catch((err) => {
            console.error("req=" + req.id + ' Error getting entity type: ', err);
            reject(err);
        });
    });
}

const updateEntity = async(req, displayName, entityList) => {
    return new Promise(async(resolve, reject) => {
        var preEntity = await getEntity(req, displayName);
        var dialog_flow_data = await dialog_flow_connection(req.company);
        for (var j = 0; j < entityList.length; j++) {
            for (var i = 0; i < preEntity.entities.length; i++) {
                if (preEntity.entities[i].value == entityList[j].value) {
                    entityList[j].synonyms.forEach(function(value) {
                        if (preEntity.entities[i].synonyms.indexOf(value) == -1) preEntity.entities[i].synonyms.push(value);
                    });
                    break;
                } else if (preEntity.entities[i].value != entityList[j].value && i == preEntity.entities.length - 1) {
                    preEntity.entities.push(entityList[j])
                }
            }
        }
        const request = {
            entityType: city,
            updateMask: {
                paths: ['entities'],
            },
        };
        dialog_flow_data.entitiesClient.updateEntityType(request).then((responses) => {
            console.log('Updated entity type:', JSON.stringify(responses[0]));
            resolve(responses[0])
        }).catch((err) => {
            console.error("req=" + req.id + ' Error getting entity type: ', err);
            reject(err);
        });
    })
}

const deleteEntity = async(req, displayName) => {
    return new Promise(async(resolve, reject) => {
        var preEntity = await getEntity(req, displayName);
        var dialog_flow_data = await dialog_flow_connection(req.company);
        const entityTypePath = dialog_flow_data.entitiesClient.entityTypePath(
            dialog_flow_data.projectId,
            preEntity.name.split("/")[preEntity.name.split("/").length - 1]
        );
        dialog_flow_data.entitiesClient.deleteEntityType({ name: entityTypePath }).then(() => {
            console.log('Delete entity type: ', displayName);
            resolve("Successfully delete");
        }).catch((err) => {
            console.error("req=" + req.id + ' Error getting entity type: ', err);
            reject(err);
        });
    })
}

async function dialog_flow_connection(company) {

    var cred = await dao.getOneFromDB("dialog_flow_creds", { company: company });
    const entitiesClient = new dialogflow.EntityTypesClient({
        credentials: cred,
    });
    const projectId = cred.project_id;
    const agentPath = entitiesClient.projectAgentPath(projectId);
    return {
        entitiesClient,
        agentPath,
        projectId
    }
}

module.exports = {
    createNewEntity,
    getEntity,
    getAllEntity,
    updateEntity,
    deleteEntity
}