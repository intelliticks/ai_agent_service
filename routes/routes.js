const dialogFlowEntity = require('../controllers/dialogFlowEntityController')

module.exports = (router) => {
    router.post('/dialog_flow_entity/:company', dialogFlowEntity.createNewEntity);
    router.get('/dialog_flow_entity/:company', dialogFlowEntity.getAllEntity);
    router.get('/dialog_flow_entity/:company/:displayName', dialogFlowEntity.getEntity);
    router.put('/dialog_flow_entity/:company', dialogFlowEntity.updateEntity);
    router.delete('/dialog_flow_entity/:company', dialogFlowEntity.deleteEntity);
}