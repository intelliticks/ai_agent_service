const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
var config = require(`../config/${process.env.AI_AGENT_SERVICE_ENV || "local"}.json`);

let cachedDb = null;

function connectToDatabaseClient() {
    if (cachedDb && cachedDb.serverConfig.isConnected()) {
        console.log('=> using cached database instance');
        return Promise.resolve(cachedDb);
    }
    return MongoClient.connect(config.mongo_url, { useNewUrlParser: true })
        .then(client => { return client; });
}
async function connectDB() {
    let client = await connectToDatabaseClient();
    return client.db(config.mongo_db);
}
const getOneFromDB = async(tableName, filter) => {
    var db = await connectDB();
    return await db.collection(tableName).findOne(filter);
}

module.exports = {
    getOneFromDB
}