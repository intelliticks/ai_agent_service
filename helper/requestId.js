var request_id = {
    id: "",
    company: "",
};

function requestId(company) {
    request_id.id = Math.random().toString().substr(2);
    request_id.company = company;
    return request_id;
}

module.exports = {
    requestId
}